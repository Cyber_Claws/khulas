import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Thumb from './../../Thumb';

class CartProduct extends Component {
  static propTypes = {
    product: PropTypes.object.isRequired,
    removeProduct: PropTypes.func.isRequired
  };

  state = {
    isMouseOver: false
  };

  handleMouseOver = () => {
    this.setState({ isMouseOver: true });
  };

  handleMouseOut = () => {
    this.setState({ isMouseOver: false });
  };

  render() {
    const { product, removeProduct } = this.props;

    const classes = ['shelf-item'];

    if (!!this.state.isMouseOver) {
      classes.push('shelf-item--mouseover');
    }

    return (
      <div className={classes.join(' ')}>
        <div
          className="shelf-item__del"
          onMouseOver={() => this.handleMouseOver()}
          onMouseOut={() => this.handleMouseOut()}
          onClick={() => removeProduct(product)}
        ><span uk-icon="close"></span></div>
        <Thumb
          classes="shelf-item__thumb"
          src={`https://khula.co.za/ProducePic/${product.name.replace(" ", "").toLowerCase()}.jpg`}
          alt={""}
        />
        <div className="shelf-item__details">
          <p className="title">{product.name}</p>
          <p className="desc">
            {`${product.packaging.split(',')[0]}`} <br />
            Quantity: {product.quantity}
          </p>
        </div>
        <div className="shelf-item__price">
          <p>{`R ${parseFloat(product.price).toFixed(2)}`}</p>
        </div>

        <div className="clearfix" />
      </div>
    );
  }
}

export default CartProduct;
