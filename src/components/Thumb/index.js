import React from 'react';
import PropTypes from 'prop-types';

const Thumb = props => {
  return (
    <div style={props.style} className={props.classes}>
      <img src={props.src} alt={props.alt} title={props.title} onerror="this.onerror=null;this.src='https://khula.co.za/images/user.png';"/>
    </div>
  );
};

Thumb.propTypes = {
  alt: PropTypes.string,
  title: PropTypes.string,
  classes: PropTypes.string,
  src: PropTypes.string.isRequired
};

export default Thumb;
