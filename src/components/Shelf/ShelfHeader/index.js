import React from 'react';
import PropTypes from 'prop-types';

import Sort from '../Sort';

const ShelfHeader = props => {
  return (
    <div className="shelf-container-header">
        <form style={{float: "left", marginRight: "50px"}} class="uk-search uk-search-default">
          <input class="uk-search-input" type="search" placeholder="Search..."/>
        </form>
      {/*<Sort />*/}
    </div>
  );
};

ShelfHeader.propTypes = {
  productsLength: PropTypes.number.isRequired
};

export default ShelfHeader;
