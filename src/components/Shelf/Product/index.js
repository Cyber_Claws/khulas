import React from 'react';
import PropTypes from 'prop-types';

import Thumb from '../../Thumb';

const Product = props => {
  const product = props.product;

  // product.quantity = 1;

  let formattedPrice = parseFloat(product.price).toFixed(2)
  return (
    <div
      className="shelf-item"
      data-sku={product.sku}>
      {product.isFreeShipping && (
        <div className="shelf-stopper">Free Rage</div>
      )}
      <Thumb
        classes="shelf-item__thumb"
        style={{minHeight: "150px"}}
        src={`https://khula.co.za/ProducePic/${product.name.replace(" ", "").toLowerCase()}.jpg`}
        alt={product.name}
      />
      <p className="shelf-item__title">{product.name}</p>
      <div className="shelf-item__price">
        <div className="val">
          <small>R</small>
          <b>{formattedPrice.substr(0, formattedPrice.length - 3)}</b>
          <span>{formattedPrice.substr(formattedPrice.length - 3, 3)}</span>
        </div>
        <div className="product-details">
          <span>per </span>
          <b>
            {product.packaging.split(',')[0]}
          </b>
        </div>
      </div>
      <div 
        onClick={() => props.addProduct(product)}
        className="shelf-item__buy-btn">
          Add to cart
      </div>
    </div>
  );
};

Product.propTypes = {
  product: PropTypes.object.isRequired,
  addProduct: PropTypes.func.isRequired
};

export default Product;
